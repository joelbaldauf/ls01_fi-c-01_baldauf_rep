import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie eine Note zwischen 1 und 6 an.");
		byte note = tastatur.nextByte();
		
		switch(note) {
		case 1:
			System.out.print("Sehr gut");
			break;
		case 2:
			System.out.print("gut");
			break;
		case 3:
			System.out.print("befriedigend");
			break;
		case 4:
			System.out.print("ausreichend");
			break;
		case 5:
			System.out.print("mangelhaft");
			break;
		case 6:
			System.out.print("ungenügend");
			break;
		default:
			System.out.print("ungültige Ziffer eingegebe. Die Ziffer muss zwischen 1-6 liegen.");
			break;
		}
		
//		if (note == 1) {
//		System.out.print("Sehr gut");
//		}
//		
//		else if (note == 2) {
//			System.out.print("gut");
//		}
//		
//		else if (note == 3) {
//			System.out.print("befriedigend");
//		}
//		
//		else if (note == 4) {
//			System.out.print("ausreichend");
//		}
//		
//		else if (note == 5) {
//			System.out.print("mangelhaft");
//		}
//
//		else if (note == 6) {
//			System.out.print("ungenügend");
//		}
//		
//		else {
//			System.out.print("ungültige Ziffer eingegebe. Die Ziffer muss zwischen 1-6 liegen.");
//		}
	}
}
