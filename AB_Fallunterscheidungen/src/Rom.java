import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Eingabe r�mische Zahl > ");
		String rZahl = tastatur.next();

		int i;
		int dezZahl = 0;
		int lZahl = 0;
		int zahl = 0;
		byte gleich = 0;
		int v = 0, l = 0, d = 0;

		for (i = 0; i < rZahl.length(); i++) {
			if (rZahl.charAt(i) == 'V') {
				v += 1;
			} else if (rZahl.charAt(i) == 'L') {
				l += 1;
			} else if (rZahl.charAt(i) == 'D') {
				d += 1;
			}
		}

		if (v > 1 || l > 1 || d > 1) {
			System.out.print("Eingabe ung�ltig!");
			return;
		}

		for (i = 0; i < rZahl.length(); i++) {
			switch (rZahl.charAt(i)) {
			case 'I':
				zahl = 1;
				break;
			case 'V':
				zahl = 5;
				break;
			case 'X':
				zahl = 10;
				break;
			case 'L':
				zahl = 50;
				break;
			case 'C':
				zahl = 100;
				break;
			case 'D':
				zahl = 500;
				break;
			case 'M':
				zahl = 1000;
				break;
			}
//			System.out.println("zahl " + zahl);
//			System.out.println("lZahl " + lZahl);
//			System.out.println("dZahl " + dezZahl);
			if (zahl > lZahl && lZahl != 0) {
				if ((lZahl == 1 && (zahl == 5 || zahl == 10)) || (lZahl == 10 && (zahl == 50 || zahl == 100))
						|| (lZahl == 100 && (zahl == 500 || zahl == 1000))) {
					dezZahl = (dezZahl - lZahl) + (zahl - lZahl);
				} else {
					System.out.print("Eingabe ung�ltig!");
					return;
				}
			} else {
				dezZahl += zahl;
			}

			if (lZahl == zahl) {
				gleich += 1;
			} else {
				gleich = 0;
			}

			if (gleich >= 3) {
				System.out.print("Eingabe ung�ltig!");
				return;
			}
			lZahl = zahl;

		}
		System.out.print(dezZahl);
	}

}
