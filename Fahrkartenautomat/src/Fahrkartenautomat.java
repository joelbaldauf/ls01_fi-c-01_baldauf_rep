﻿import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

class Fahrkartenautomat
{

	private static int ticketRohlinge = 5;

	// Anzahl der vorrätigen 2 EUR, 1 EUR, 50 CENT, 20 CENT, 10 CENT, 5 CENT Münzen
	private static final double[] wertMuenzen = {2, 1, 0.5, 0.2, 0.1, 0.05};
	private static int[] anzahlMuenzen = {3, 3, 3, 3, 3, 3};

	public static double auswahlErfassen (){
		//===================================================================
		// Array für die Bezeichnungen

		String[] bezeichnungen =  {"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC", 
				"Einzelfahrschein Berlin ABC", 
				"Kurzstrecke", 
				"Tageskarte Berlin AB", 
				"Tageskarte Berlin BC", 
				"Tageskarte Berlin ABC", 
				"Kleingruppen-Tageskarte Berlin AB", 
				"Kleingruppen-Tageskarte Berlin BC", 
		"Kleingruppen-Tageskarte Berlin ABC"};

		// Array für die jeweiligen Preise
		double[] preise = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
		//===================================================================

		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag = 0.0;
		double einzelBetrag = 0.0;

		byte auswahlTicket = 0;

		while (true) {
			System.out.printf("\nWählen Sie:\n");
			int i=0;
			for (; i<bezeichnungen.length; i++)
				System.out.printf("  %s [%.2f EUR] (%d)\n", bezeichnungen[i], preise[i], i+1);
			System.out.printf("  Bezahlen [%.2f EUR] (%d)\n", zuZahlenderBetrag,i+1);
			System.out.printf("  Stornieren (%d)\n", i+2);
			System.out.print("Ihre Wahl: ");
			try {
				auswahlTicket = tastatur.nextByte();
			}
			catch(InputMismatchException e) {
				auswahlTicket = 0;
				tastatur.nextLine();
			}
			try {
				einzelBetrag = preise[auswahlTicket-1];
			}
			catch (ArrayIndexOutOfBoundsException exception) {
				if (auswahlTicket == i+1) {
					break;
				}
				else if (auswahlTicket == i+2) {
					zuZahlenderBetrag = 0.0;
					einzelBetrag = 0.0;
					continue;
				}
				else if (auswahlTicket == 99) 
				{
					admin();
					break;
				}
				else {System.out.println(" >>falsche Eingabe<<");  warte(1000); continue;}
			}
			zuZahlenderBetrag += anzahlErfassen(einzelBetrag);
		}
		return zuZahlenderBetrag;
	}

	public static double anzahlErfassen(double einzelBetrag) {
		Scanner tastatur = new Scanner(System.in);
		byte anzahlTickets = 0;

		while (true) {
			System.out.print("Anzahl der Tickets: ");
			try {
				anzahlTickets = tastatur.nextByte();
			}
			catch(InputMismatchException e) {
				anzahlTickets = 0;
				tastatur.nextLine();
			}
			if (!(anzahlTickets > 0 && anzahlTickets <= 10)) {
				System.out.print("Es wurde ein ungültiger Wert für die Ticketanzahl eingegeben. \n"
						+ "Bitte erneut versuchen.\n");
				continue;
			}
			break;
		}
		ticketRohlinge-=anzahlTickets;
		return anzahlTickets * einzelBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		if (zuZahlenderBetrag == 0) {
			System.out.println("Nichts zu Bezahlen");
			warte(1000);
			main(null);
		}

		Scanner tastatur = new Scanner(System.in);
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = 0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			double eingeworfeneMuenze = 0.0;
			//Liste mit gültigen Münzen
			List<Double> muenzen = Arrays.asList(2.0, 1.0, 0.5, 0.2, 0.1, 0.05);
			while (true) {
				System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
				try {
					eingeworfeneMuenze = tastatur.nextDouble();
				}
				catch(InputMismatchException e) {
					eingeworfeneMuenze = 0;
					tastatur.nextLine();
				}
				//Muenze prüfen
				if (!muenzen.contains(eingeworfeneMuenze)) {
					System.out.println("Es wurde eine ungültige Münze eingeworfen! Bitte erneut versuchen");
				}
				//wenn Münze gültig kann die Schleife verlassen werden
				else break;
			}
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
			anzahlMuenzen[muenzen.indexOf(eingeworfeneMuenze)] += 1;
		}
		// Rückgeldberechnung
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rueckgabebetrag;
	}

	public static void fahrkartenAusgeben () {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldausgabe
		// ------------------------------
		if(rückgabebetrag > 0.0)
		{
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			List<Muenze> rueckgabeMuenzen = new ArrayList<Muenze>();

			while(rückgabebetrag > 1.9 && anzahlMuenzen[0] > 0) // 2 EURO-Münzen
			{
				anzahlMuenzen[0] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(2, "EURO"));
				rückgabebetrag -= 2.0;
			}
			while(rückgabebetrag > 0.9 && anzahlMuenzen[1] > 0) // 1 EURO-Münzen
			{
				anzahlMuenzen[1] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(1, "EURO"));
				rückgabebetrag -= 1.0;
			}
			while(rückgabebetrag > 0.49 && anzahlMuenzen[2] > 0) // 50 CENT-Münzen
			{
				anzahlMuenzen[2] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(50, "CENT"));
				rückgabebetrag -= 0.5;
			}
			while(rückgabebetrag > 0.19 && anzahlMuenzen[3] > 0) // 20 CENT-Münzen
			{
				anzahlMuenzen[3] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(20, "CENT"));
				rückgabebetrag -= 0.2;
			}
			while(rückgabebetrag > 0.09 && anzahlMuenzen[4] > 0) // 10 CENT-Münzen
			{
				anzahlMuenzen[4] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(10, "CENT"));
				rückgabebetrag -= 0.1;
			}
			while(rückgabebetrag > 0.049 && anzahlMuenzen[5] > 0)// 5 CENT-Münzen
			{
				anzahlMuenzen[5] -= 1;
				rueckgabeMuenzen.add(muenzeErstellen(5, "CENT"));
				rückgabebetrag -= 0.05;
			}
			if (rueckgabeMuenzen.size() > 0)
				muenzenAusgeben(rueckgabeMuenzen);
			else
				errorAnzeigen(2);
		}
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Muenze muenzeErstellen(int betrag, String einheit) {
		Muenze addMuenze = new Muenze(betrag, einheit);
		return addMuenze;
	}

	public static void muenzenAusgeben(List<Muenze> muenzen) {
		int anzahlZeilen = (int)((double)muenzen.size()/(double)3+0.9);
		int[] anzahlMuenzen = new int[anzahlZeilen];
		int fMuenzen=0;

		for (int i=0; i<anzahlZeilen-1; i++) {anzahlMuenzen[i] = 3; fMuenzen++;}
		if (anzahlZeilen == 1) anzahlMuenzen[0] = muenzen.size();
		else anzahlMuenzen[anzahlZeilen-1] = muenzen.size() - fMuenzen;

		fMuenzen=0;
		for (int i=0; i<anzahlZeilen; i++) {
			System.out.println("   * * *     ".repeat(anzahlMuenzen[i]));
			System.out.println(" *       *   ".repeat(anzahlMuenzen[i]));
			String zBetrag = "";
			String zEinheit = "";
			int j=0;
			for (; (j<anzahlMuenzen[i]) || j>3; j++) {
				zBetrag += String.format("*    %-2d   *  ", muenzen.get(j+fMuenzen).getBetrag());
				zEinheit += String.format("*   %-4s  *  ", muenzen.get(j+fMuenzen).getEinheit());
			}
			System.out.println(zBetrag);
			System.out.println(zEinheit);
			System.out.println(" *       *   ".repeat(anzahlMuenzen[i]));
			System.out.println("   * * *     ".repeat(anzahlMuenzen[i]));
			fMuenzen+=j;
		}
	}

	public static void verabschiedungAnzeigen () {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir wünschen Ihnen eine gute Fahrt.\n\n");
		warte(4000);
	}

	public static void admin() {
		Scanner tastatur = new Scanner(System.in);
		byte auswahl;
		boolean imGange = true;
		System.out.println("Administrationsmenü\n============");
		System.out.printf("verbleibende Ticketrohlinge: %d\n", ticketRohlinge);
		System.out.printf("Kassenstand: %.2f EUR\n", kassenstandGes());
		kassenstandM();

		while (imGange) {
			System.out.println("Kasse leeren (1)");
			System.out.println("Kasse auffüllen (2)");
			System.out.println("Ticketrohlinge auffüllen (3)");
			System.out.println("Adminmenü verlassen (4)");
			System.out.print("Ihre Wahl: ");
			try {
				auswahl = tastatur.nextByte();
			}
			catch(InputMismatchException e) {
				auswahl = 0;
				tastatur.nextLine();
			}

			switch(auswahl) {
			case 1:
				kasseLeeren();
				imGange = false;
				break;
			case 2:
				kasseAuffuellen();
				imGange = false;
				break;
			case 3:
				ticketsAuffuellen();
				imGange = false;
				break;
			case 4:
				imGange = false;
				break;
			default:
				System.out.println(" >>falsche Eingabe<<");
				warte(1000);
			}
		}
	}

	public static void errorAnzeigen(int error) {
		switch (error) {
		case 1:
			System.out.println("\n\n\n\n\n\n\n\nSTÖRUNG\n"
					+ "=======\n" + "Die Ticketrohlinge sind leider aufgebraucht.\nWir bitten um Entschuldigung.\nTechniker erreichbar unter 030 12345" );
			System.exit(0);
			break;
		case 2:
			System.out.println("\n\n\n\n\n\n\n\nSTÖRUNG\n"
					+ "=======\n" + "Es konnte kein passendes Wechselgeld ausgegeben werden.\nIhr eingegebenes Geld wurde zurückgegeben.\n"
							+ "Ein Ticket wurde nicht ausgegeben.\nWir bitten um Entschludigung\nTechniker erreichbar unter 030 12345");
			System.exit(0);
		}
	}

	public static void kassenstandM() {
		for (int i = 0; i < anzahlMuenzen.length; i++) {
			if (wertMuenzen[i] >= 1)
				System.out.printf("%.0f EUR-Münzen: %d Stück\n", wertMuenzen[i], anzahlMuenzen[i]);
			else {
				int wert = (int) ((double)(wertMuenzen[i])*(double)100);
				System.out.printf("%d CENT-Münzen: %d Stück\n", wert, anzahlMuenzen[i]);
			}
		}
	}
		

	public static double kassenstandGes() {
		double kassenstand = 0.0;
		for (int i = 0; i < anzahlMuenzen.length; i++) {
			for (int j = 0; j < anzahlMuenzen[i] ; j++) 
				kassenstand += wertMuenzen[i];
		}
		return kassenstand;
	}

	public static void kasseLeeren() {
		System.out.println("Kasse wurde geleert");
		for (int i = 0; i < anzahlMuenzen.length; i++) {
			anzahlMuenzen[i] = 0;
		}
		admin();
	}


	public static void kasseAuffuellen() {
		Scanner tastatur = new Scanner(System.in);
		int hinzu = 0;
		for (int i = 0; i < anzahlMuenzen.length; i++) {
			if (wertMuenzen[i] >= 1)
				System.out.printf("Wieviele %.0f EUR-Münzen sollen hinzugefügt werden:", wertMuenzen[i]);
			else {
				int wert = (int) ((double)(wertMuenzen[i])*(double)100);
				System.out.printf("Wieviele %d CENT-Münzen sollen hinzugefügt werden: ", wert);
			}
			while(true) {
				try {
					hinzu = tastatur.nextInt();
				}
				catch(InputMismatchException e) {
					hinzu = 0;
					tastatur.nextLine();
				}
				if (hinzu > 0)  {
					anzahlMuenzen[i] += hinzu;
					break;
				}
				else System.out.println("ungültige Eingabe\n");
					
			}
		}
		admin();
	}



	public static void ticketsAuffuellen() {
		Scanner tastatur = new Scanner(System.in);
		int ticketsHinzu = 0;
		while(true) {
			System.out.print("Wieviele Rohlinge sollen aufgefüllt werden?: ");
			try {
				ticketsHinzu = tastatur.nextInt();
			}
			catch(InputMismatchException e) {
				ticketsHinzu = 0;
				tastatur.nextLine();
			}
			if (ticketsHinzu > 0)  {
				ticketRohlinge += ticketsHinzu;
				admin();
				break;
			}
			else System.out.println("ungültige Eingabe\n");
				
		}
	}


	public static void main(String[] args)
	{      

		double zuZahlenderBetrag; 
		double rueckgabebetrag;

		while (ticketRohlinge > 0) {
			zuZahlenderBetrag = auswahlErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			rueckgeldAusgeben(rueckgabebetrag);
			fahrkartenAusgeben();
			verabschiedungAnzeigen();
		}
		errorAnzeigen(1);
	}


}

class Muenze {
	private int betrag;
	private String einheit;

	public Muenze(int betrag, String einheit) {
		this.setBetrag(betrag);
		this.einheit=einheit;
	}

	public int getBetrag() {
		return betrag;
	}

	public void setBetrag(int betrag) {
		this.betrag = betrag;
	}

	public String getEinheit() {
		return einheit;
	}

	public void setEinheit(String einheit) {
		this.einheit = einheit;
	}
}