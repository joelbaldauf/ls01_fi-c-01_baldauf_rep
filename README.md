## Fahrkartenautomat V 2020.01.10
---

*Besonderheiten*  
- Adminmenü über die Eingabe von 99 während der Ticketauswahl erreichbar  
- über das Adminmenü ist die Verwaltung der Kasse (leeren und füllen mit Münzen) möglich  
- Tickets werden nur bei genügend Rohlingen ausgegeben, sonst ERROR  
- Wechselgeld wird enstprechend verfügbarer Münzen ausgegeben, sonst ERROR