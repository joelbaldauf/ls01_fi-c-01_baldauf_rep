import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner (System.in);
		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		byte zahl = tastatur.nextByte();
		
		int i = 0;
		int j = 0;
		for (int z = 0; z<10; z++) {
			j = i;	
			for (; i<j+10; i++) {
					if ((i % 10 == zahl || i % zahl == 0 || berechneQuersumme(i) == zahl) && i!=0) System.out.printf("%5s", "*");
					else System.out.printf("%5d", i);
					
				}
			System.out.print("\n");
		}
	}
	public static int berechneQuersumme(int zahl) {
		int summe = 0;
		while (zahl != 0) {
			summe += zahl % 10;
			zahl /= 10;
		}
		return summe;
	}

}
