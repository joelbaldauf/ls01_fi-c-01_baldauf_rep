public class Treppe {

	public static void main(String[] args) {
		int h=5, b=3;
		
		for (int i=1; i<=h; i++) {
			String repeatedStars =  "*".repeat(b);
			System.out.printf("%"+b*h+"s\n",repeatedStars.repeat(i));	
		}
	}
}