import java.util.Scanner; //1
import java.util.Random;

public class Mittelwert {

	public static int benutzereingabe(String text) {
		Scanner sc = new Scanner(System.in);
		System.out.print(text);
		int y = sc.nextInt();
		return y;
	}

	public static int[] zufallszahlenArray(int anzahl) {
		Random rd = new Random();
		int[] zufallArray = new int[anzahl];

		for (int i = 0; i < anzahl; i++) {
			zufallArray[i] = rd.nextInt(99);
		}
		return zufallArray;
	}

	public static double mittelwertBerechnung(int[] meinArray) {
		int summe = 0;
		double mittel = 0.0;

		for (int i = 0; i < meinArray.length; i++) {
			summe += meinArray[i];
		}
		mittel = (double)summe / meinArray.length;
		return mittel;
	}

	public static void ausgabeArray(int[] meinArray) {
		for (int i = 0; i < meinArray.length; i++) {
			System.out.printf("%3d", meinArray[i]);
		}
	}

	public static void ausgabeMittelwert(double mittelwert) {
		System.out.printf("\nDer Mittelwert ist: %.2f", mittelwert);
	}
	
	public static void main(String[] args) {
		int anzahl;
		int[] zahlenArray;  
		double mittelwert;

		anzahl = benutzereingabe("Wie viele Zahlen soll das Array enthalten: ");
		zahlenArray = zufallszahlenArray(anzahl);
		mittelwert = mittelwertBerechnung(zahlenArray);
		ausgabeArray(zahlenArray);
		ausgabeMittelwert(mittelwert);
	}
}
